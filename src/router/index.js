import Vue from 'vue';
import Router from 'vue-router';
import MainPage from '@/pages/MainPage';
import ProductPage from '@/pages/ProductPage';
import Products from '@/pages/Products';
import FullCatalog from '@/pages/FullCatalog';
import Cart from '@/pages/Cart';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage,
    },
    {
      path: '/product/:id',
      name: 'ProductPage',
      component: ProductPage,
      props: true,
    },
    {
      path: '/products',
      name: 'Products',
      component: Products,
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart,
    },
    {
      path: '/fullCatalog/',
      name: 'FullCatalog',
      component: FullCatalog,
    },
  ],
});
