import Vue from 'vue';
import Vuex from 'vuex';
// import { devtoolsMiddleware } from 'vuex-devtools';

Vue.use(Vuex);

const items = [];
for (let i = 0; i < 8; i++) { // eslint-disable-line
  const b = i + 1;
  items.push({
    id: i,
    img: `/static/catalog/${b}.png`,
    name: `Product #${i}`,
    oldPrice: 29.99,
    price: 19.99,
    description: 'Sed et repudiare intellegam, sonet melius mandamus cu mel. Ut tation exerci appellantur vix, impetus salutandi intellegat no pri, quis euismod accusam ius ut. Eum nostro accumsan eu, exerci propriae vulputate ex quo, id usu vide doming audire. Reprimique eloquentiam pro ad, euripidis efficiantur intellegebat an ius, ea nec delicata corrumpit. Ea eam viderer similique adipiscing, est quis decore reprehendunt at. Lucilius tincidunt persecuti sea eu, quod ponderum vituperata duo ad, libris aperiam expetendis vis te. Rebum laudem nostrum pro ea, meis option praesent mel cu.',
    stars: i + 1,
  });
}

export default new Vuex.Store({
  state: {
    blur: true,
    items: items,
    cart: [],
  },
  mutations: {
    addItem (state, item) {
      const isInCart = state.cart.find((c) => c.id === item.id)
      if (isInCart) {
        isInCart.count++
      } else {
        const newItem = {...item, ...{count: 1}}
        state.cart.push(newItem)
      }
    },
    removeItem (state, item){
      const isInCart = state.cart.find((c) => c.id === item.id)
      if (isInCart.count > 1){
        isInCart.count--
      }
      else{
        state.cart = state.cart.filter((c) => c.id !== item.id)
      }
    },
    blurToggle (state, blur = !state.blue) {
      state.blur = blur
    }
  },
  getters: {
    getItems(state) {
        return state.items;
    },
    getCart(state) {
      return state.cart;
    },
    getBlurState(state) {
      return state.blur;
    }
  },
})
